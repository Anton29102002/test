<?php

namespace App\Console\Commands;

use App\Jobs\RequestReviewJob;
use DefStudio\Telegraph\Models\TelegraphChat;
use Illuminate\Console\Command;

class RequestReview extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:request-review';
    protected $time = 0;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
           TelegraphChat::query()->where('has_review',false)->chunk(30, function ($chunk) {
               foreach ($chunk as $key => $chat) {
                   RequestReviewJob::dispatch($chat)->delay(($this->time+$key)*5);
                   $this->time =+1;
               }
           });
    }
 }
