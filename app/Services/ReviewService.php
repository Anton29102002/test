<?php

namespace App\Services;

use App\Enums\ReviewType;
use App\Models\Review;
use DefStudio\Telegraph\Telegraph;
use Illuminate\Support\Facades\Storage;

class ReviewService
{
    public static function saveAttribute(int $telegraph_chat_id, array $message)
    {
        $review = new Review();
        $review->telegraph_chat_id = $telegraph_chat_id;

        if (isset($message['voice'])) {
            $fileId = $message['voice']['file_id'];
            $filePath = self::saveFile($fileId);
            $review->review = $filePath;
            $review->type = ReviewType::audio->name;
        }

        if (isset($message['video_note'])) {
            $fileId = $message['video_note']['file_id'];
            $filePath = self::saveFile($fileId);
            $review->review = $filePath;
            $review->type = ReviewType::video->name;
        }

        if (isset($message['text'])) {
            $review->review = $message['text'];
            $review->type = ReviewType::text->name;
        }

        $review->save();

        return $review;
    }

    private static function saveFile(string $fileId): string
    {
        $storagePath = Storage::path('bot/reviews');
        $telegraph = new Telegraph();
        $filePath = $telegraph->store($fileId, $storagePath);

        return $filePath;
    }

}
