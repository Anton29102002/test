<?php

namespace App\Services;

use App\Models\User;

class UserService
{
    public static function saveAttribute(int $telegraph_chat_id, array $attributes)
    {
        $user = User::query()->firstOrCreate(['telegraph_chat_id' => $telegraph_chat_id]);
        $user->fill($attributes);
        $user->save();
        return $user;
    }

}
