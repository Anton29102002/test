<?php

namespace App\Enums;

enum ReviewType
{
    case video;
    case text;
    case audio;
}
