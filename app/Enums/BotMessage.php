<?php

namespace App\Enums;

enum BotMessage :string
{
    case name = 'Введите ваше имя';
    case last_name = 'Введите вашу фамилию';
    case surname = 'Введите ваше отчество';
    case email = 'Введите вашу почту';
    case phone_number = 'Введите ваш номер телефона';
    case birth_date = 'Введите дату вашего рождения';
    case send_review = 'Пожалуйста отправьте отзыв';
    case greeting = 'Добро пожаловать, данный бот является тестовым заданием';
    case time = 'Введите часовой пояс';
    case completed = 'Регистрация прошла успешно ✅';
    case completed_review = 'Отзыв сохранен✅';
}
