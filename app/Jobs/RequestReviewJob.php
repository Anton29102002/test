<?php

namespace App\Jobs;

use App\Enums\BotMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RequestReviewJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public  $chat;

    public function __construct($chat)
    {
       $this->chat = $chat;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->chat->message(BotMessage::send_review->value)->forceReply()->send();
    }
}
