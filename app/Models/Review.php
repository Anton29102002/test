<?php

namespace App\Models;

use DefStudio\Telegraph\Telegraph;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;

    public function chats()
    {
        return $this->belongsTo(Telegraph::class);
    }
}
