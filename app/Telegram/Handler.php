<?php

namespace App\Telegram;

use App\Enums\BotMessage;
use App\Services\ReviewService;
use App\Services\UserService;

class Handler extends \DefStudio\Telegraph\Handlers\WebhookHandler
{

    public function start()
    {
        $this->chat->message(BotMessage::greeting->value)->send();
        if ($this->chat->user_id === null) {
            return $this->getInformation(BotMessage::name->value);
        }
    }

    public function test()
    {
        return $this->getInformation(BotMessage::send_review->value);
    }

    private function getInformation(string $message)
    {
        return $this->chat->message($message)->forceReply()->send();
    }

    public function handleChatMessage(\Illuminate\Support\Stringable $text): void
    {
        $replyText = $this->request['message']['reply_to_message']['text'] ?? '';
        switch ($replyText) {
            case BotMessage::name->value:
                $model = UserService::saveAttribute($this->chat->id, [
                    'name' => $text,
                    'locale' => $this->request['message']['from']['language_code']
                ]);

                $this->chat->user_id = $model->id;
                $this->chat->save();

                $this->getInformation(BotMessage::last_name->value);
                break;
            case BotMessage::last_name->value:
                UserService::saveAttribute($this->chat->id, ['last_name' => $text]);
                $this->getInformation(BotMessage::surname->value);
                break;
            case BotMessage::surname->value:
                UserService::saveAttribute($this->chat->id, ['surname' => $text]);
                $this->getInformation(BotMessage::phone_number->value);
                break;
            case BotMessage::phone_number->value:
                UserService::saveAttribute($this->chat->id, ['phone_number' => $text]);
                $this->getInformation(BotMessage::email->value);
                break;
            case BotMessage::email->value:
                UserService::saveAttribute($this->chat->id, ['email' => $text]);
                $this->getInformation(BotMessage::birth_date->value);
                break;
            case BotMessage::birth_date->value:
                UserService::saveAttribute($this->chat->id, ['birth_date' => $text]);
                $this->getInformation(BotMessage::time->value)->send();
                break;
            case BotMessage::time->value:
                UserService::saveAttribute($this->chat->id, ['timezone' => $text]);
                $this->chat->message(BotMessage::completed->value)->send();
                break;
            case BotMessage::send_review->value:
                ReviewService::saveAttribute($this->chat->id, $this->request['message']);
                $this->chat->message(BotMessage::completed_review->value)->send();
                $this->chat->has_review = true;
                $this->chat->save();
                break;
            default:
                break;
        }

    }
}
