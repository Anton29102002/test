# Test work
**Локальная разработка**

- Установка:
    - `git clone https://gitlab.com/Anton29102002/test.git`
    - `cd Test`
    - `cp .env.example .env`
    - `docker run --net=host -it -e NGROK_AUTHTOKEN=2Zr4fk6lp1UEw9rzOAloxH6gBRv_2rgEAmte5r2Kst2aqc8Me ngrok/ngrok:latest http 90`
    -  Копируем url в .env в корне (APP_URL)
    - `cd dockerLocal`
    - `cp .env.example .env`
    - `docker-compose build`
    - `docker-compose up -d`
    - `docker-compose exec php-fpm bash`

- Команды: для выполниия команд внутри контейнера
  ```
    php artisan migrate --force  запустить миграции
    php artisan db:seed --class=BotSeeder
    php artisan telegraph:set-webhook
    php artisan horizon
    ```
- ссылка на бота https://t.me/fit_00bot
