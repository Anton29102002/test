<?php

namespace Database\Seeders;

use DefStudio\Telegraph\Models\TelegraphBot;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class BotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TelegraphBot::query()->create([
            'token' => '6849758242:AAGM83B9ht4aZ1StRPcYiVTxy-P29MchpZo',
            'name' => 'test_bot'
        ]);
    }
}
